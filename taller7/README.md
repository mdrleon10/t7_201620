�Es	posible	lograr	el	mismo	resultado	con	el	pre-orden	y	pos-orden?

No no es posible obtener los mismos resultados con el preorden y el pos- orden ya que se leen los datos de manera diferente. El preorden comienza en la parte de arriba del arbol y lee de izquierda a derecha. El pos-orden mpieza desde la parte de abajo del arbol y sube leyendo de izquierda a derecha.

�Es	posible	lograr	el	mismo	resultado	con	el	in-orden	y	pos-orden?

No no se obtendra el mismo resultado pues el in_orden lee el arbol desde la hoja m�s a la izquierda, luego pasa por el padre y se devuelve al hijo de la derecha, y asi con cada subarbol, mientras que el pos-orden lee por niveles de izquierda a derecha. Es decir empieza por las hoja y va leyendo horizontalmente.