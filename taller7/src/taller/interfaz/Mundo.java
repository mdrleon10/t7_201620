package taller.interfaz;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import taller.interfaz.IReconstructorArbol;

public class Mundo implements IReconstructorArbol
{
	private String[] inorden;
	private String[] preorden;
	private Nodo raiz;
	private class Nodo<T>
	{
		private Nodo left;
		private Nodo right;
		private int size = 0;
		private T value;
		
		public Nodo(T val)
		{
			this.value = val;
		}
	}
	public Mundo(String nombre) throws IOException
	{
		inorden = new String[9];
		preorden = new String[9];
		cargarArchivo(nombre);
	}
	@Override
	public void cargarArchivo(String nombre) throws IOException 
	{
		System.out.println("Cargando archivo");
		System.out.println(nombre);
		File archivo = new File("./data/"+ nombre);
		// TODO Auto-generated method stub
		Properties prop = new Properties();
		FileInputStream in = new FileInputStream(archivo);
		prop.load(in);
		preorden = prop.getProperty("preorden").split(",");
		inorden = prop.getProperty("inorden").split(",");
		System.out.println(preorden.length);
		System.out.println(inorden.length);
		in.close(); 
	}
	@Override
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException 
	{
		// TODO Auto-generated method stub


		JSONArray json = new JSONArray();

		JSONObject raicita = new JSONObject();
		try {
			raicita.put("arbol", raiz);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		json.put(raicita);

		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.create();
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					new File(info)));
			dos.writeBytes(gson.toJson(json).replace("\n", "\r\n"));
			dos.flush();
			dos.close();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		 
	}

	@Override
	public void reconstruir()
	{
		// TODO Auto-generated method stub
		int preStart = 0;
		int preEnd = preorden.length-1;
		int inStart = 0;
		int inEnd = inorden.length-1;
		raiz = construct(preorden, preStart, preEnd, inorden, inStart, inEnd);
		System.out.println("hola");
	}
	public Nodo<String> construct(String[] preorden, int preStart, int preEnd, String[] inorden, int inStart, int inEnd)
	{
		if(preStart>preEnd||inStart>inEnd)
		{
			return null;
		}
		String val = preorden[preStart];
		Nodo n = new Nodo<String>(val);
		int k=0;
		for(int i=0; i<inorden.length; i++)
		{
			if(val.equals(inorden[i]))
			{
				k=i;
				break;
			}
		}
		n.left = construct(preorden, preStart+1, preStart+(k-inStart), inorden, inStart, k-1);
		n.right = construct(preorden, preStart+(k-inStart)+1, preEnd, inorden, k+1 , inEnd);
		return n;
	}
}

